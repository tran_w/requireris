import os

import hmac, base64, struct, hashlib, time, binascii
from flask import Flask
from flask import render_template, url_for, make_response, request
from flask import jsonify

app = Flask(__name__)

## ERROR HANDLING
class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.errorhandler(500)
def internal_error(error):
    return make_response(jsonify({'error': 'bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

## OTP METHODS
@app.route('/hotp/<secret>/<intervals_no>')
def get_hotp_token(secret, intervals_no):
    try:
        base64.decodestring(secret)
        key = base64.b32decode(secret, True)
        msg = struct.pack(">Q", intervals_no)
        h = hmac.new(key, msg, hashlib.sha1).digest()
        o = ord(h[19]) & 15
        h = (struct.unpack(">I", h[o:o + 4])[0] & 0x7fffffff) % 1000000
        return h
    except binascii.Error:
        raise InvalidUsage("bad request", status_code=400)


@app.route('/totp/<secret>', methods=['GET'])
def get_totp_token(secret):
    return str(get_hotp_token(secret, intervals_no=int(time.time()) // 30))

## INDEX PAGE
@app.route('/', methods=['GET', 'POST'])
def index():
    stylesheets = []
    scripts = []

    stylesheets.append(url_for('static', filename='css/bootstrap.min.css'))
    stylesheets.append(url_for('static', filename='css/style.css'))
    scripts.append('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js')
    scripts.append(url_for('static', filename='js/bootstrap.min.js'))

    if request.method == 'POST':
        return render_template('show.html',
                           secret=get_totp_token(request.form['secret']),
                           stylesheets=stylesheets,
                           scripts=scripts)

    return render_template('index.html',
                           stylesheets=stylesheets,
                           scripts=scripts)

if __name__ == '__main__':
    app.run(debug=True)
